package com.chaiwat.week10;

public class Triangle extends Shape{
    private double a;
    private double b;
    private double c;

    public Triangle(Double a, Double b, Double c){
        super("Triangle");
            this.a = a;
            this.b = b;
            this.c = c;
            
        }
        @Override
        public String toString(){
        return this.getName() + " a:"+this.a + " b:" + this.b + " c:" + this.c;
        }
        @Override
        public double calArea() {
            double s = ((a + b + c) / 2);
            return Math.sqrt(s * (s - a) * (s - b) * (s - c));
        }
        @Override
        public double calPerimeter() {
            return a + b +c;
        }
    }

